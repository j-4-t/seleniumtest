package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Homepage {
    private final WebDriver driver;

    public Homepage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get("http://www.bol.com/");

    }

    public void searchFor(String search) {
        WebElement searchInput = driver.findElement(By.id("searchfor"));
        searchInput.sendKeys(search);

        WebElement searchButton = driver.findElement(By.className("tst_headerSearchButton"));
        searchButton.click();
    }


    public String getPriceFirstResult() {
        WebElement firstResult = driver.findElement(By.className("tst_searchresult_1"));
        return firstResult.findElement(By.className("price-normal")).getText();
    }
}
