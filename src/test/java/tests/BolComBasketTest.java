package tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.Homepage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class BolComBasketTest {
    private static WebDriver driver;

    @BeforeClass
    public static void before() {
        // Download chromedriver from http://chromedriver.storage.googleapis.com/index.html
        System.setProperty("webdriver.chrome.driver", "./chromedriver");

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


    @Test
    public void testAddGreenEgg() throws InterruptedException {
        // Open homepage

        // Search for green egg

        // Add first result to basket, button inside first result has class "js_floating_basket_btn"
        // (Maybe you need to wait for 3 seconds before clicking.. Use Thread.sleep(3000); for this)

        // Open basket (find out which url)
        // (if your basket is still empty, consider waiting for 3 seconds before going to a new url)

        // Check total price, by class "tst_total_price"
    }

    @AfterClass
    public static void after() {
        driver.close();
        driver.quit();
    }

}
