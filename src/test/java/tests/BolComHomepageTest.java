package tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageobjects.Homepage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class BolComHomepageTest {
    private static WebDriver driver;

    @BeforeClass
    public static void before() {
        // Download chromedriver from http://chromedriver.storage.googleapis.com/index.html
        System.setProperty("webdriver.chrome.driver", "./chromedriver");

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testSearchGreenEgg() {
        Homepage page = new Homepage(driver);

        page.open();
        page.searchFor("green egg");

        String priceText = page.getPriceFirstResult();

        assertEquals("Price of the green egg", "1.000,00", priceText);
    }


    @AfterClass
    public static void after() {
        driver.close();
        driver.quit();
    }


}
